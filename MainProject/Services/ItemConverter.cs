using System.Collections.Generic;
using MainProject.Contracts.Entities;
using MainProject.Contracts.Entities.ValueObjects;
using MainProject.Contracts.Externals;

namespace MainProject.Services;

internal sealed class ItemConverter
{
    public IEnumerable<Item> ConvertItems(IEnumerable<ItemDto> dtoItems)
    {
        var items = new List<Item>();

        foreach (var itemDto in dtoItems)
        {
            var price = new Price
            {
                Currency = itemDto.PriceCurrency,
                Value = itemDto.PriceValue
            };
            var volumeWeight = new VolumeWeightData
            {
                Height = itemDto.Height,
                Length = itemDto.Length,
                Weight = itemDto.Weight,
                Width = itemDto.Width,
                PackagedHeight = itemDto.PackagedHeight,
                PackagedLength = itemDto.PackagedLength,
                PackagedWeight = itemDto.PackagedWeight,
                PackagedWidth = itemDto.PackagedWidth
            };
            var saleInfo = new SaleInfo
            {
                Rating = itemDto.Rating,
                IsActive = itemDto.IsActive,
                IsBestSeller = itemDto.IsBestSeller
            };

            items.Add(new Item
            {
                Id = itemDto.Id,
                Price = price,
                Sellers = GetSellers(itemDto),
                VolumeWeight = volumeWeight,
                SaleInfo = saleInfo
            });
        }

        return items;
    }

    private IEnumerable<Seller> GetSellers(ItemDto itemDto)
    {
        var sellers = new List<Seller>();
        //for (var i = 0; i < itemDto.Length; i++) sellers[i] = new Seller {Id = itemDto.SellerIds[i]};
        foreach (var sellerId in itemDto.SellerIds) sellers.Add(new Seller {Id = sellerId});
        return sellers;
    }
}