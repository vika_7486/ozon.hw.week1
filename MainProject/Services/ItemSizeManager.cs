using System.Linq;
using MainProject.Contracts.Entities.ValueObjects;
using MainProject.Enums;

namespace MainProject.Services;

internal static class ItemSizeManager
{
    public static SizeType GetSizeType(VolumeWeightData data)
    {
        var allSize = new[]
            {data.Height, data.Length, data.Width, data.PackagedHeight, data.PackagedLength, data.PackagedWidth};

        var maxSize = allSize.Max();

        if (maxSize > 100) return SizeType.Max;

        if (new[] {data.PackagedWeight, data.Weight}.Max() > 10000) return SizeType.Max;

        var packagedVolume = data.PackagedHeight * data.PackagedLength * data.PackagedWidth;
        var volume = data.Height * data.Length * data.Width;

        var minVolume = new[] {packagedVolume, volume}.Min();

        if (minVolume < 10000) return SizeType.Small;

        if (minVolume < 30000) return SizeType.Medium;

        return SizeType.Max;
    }
}