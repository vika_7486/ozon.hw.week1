﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using MainProject.Contracts.Externals;
using MainProject.Services;

namespace MainProject;

public class Program
{
    private static void Main(string[] args)
    {
        var summary = BenchmarkRunner.Run(typeof(Program).Assembly);
    }

    [MemoryDiagnoser]
    public class IntroSetupCleanupGlobal
    {
        private IEnumerable<ItemDto> _dataFromExternalService;

        private ItemDto GetByIndex(int value)
        {
            var index = value % 5;

            var itemDto = index switch
            {
                0 => JsonSerializer.Deserialize<ItemDto>(ItemDtoGenerator.RawDataItem0),
                1 => JsonSerializer.Deserialize<ItemDto>(ItemDtoGenerator.RawDataItem1),
                2 => JsonSerializer.Deserialize<ItemDto>(ItemDtoGenerator.RawDataItem2),
                3 => JsonSerializer.Deserialize<ItemDto>(ItemDtoGenerator.RawDataItem3),
                4 => JsonSerializer.Deserialize<ItemDto>(ItemDtoGenerator.RawDataItem4),
                _ => throw new Exception("Unknown index")
            };

            itemDto!.Id = value;

            return itemDto;
        }

        [GlobalSetup]
        public void GlobalSetup()
        {
            var list = new List<ItemDto>();
            foreach (var i in Enumerable.Range(1, 5000)) list.Add(GetByIndex(i));

            _dataFromExternalService = list;
        }

        [Benchmark]
        public void Process()
        {
            var converter = new ItemConverter();

            var items = converter.ConvertItems(_dataFromExternalService);

            var itemsBySize = items
                .GroupBy(i => ItemSizeManager.GetSizeType(i.VolumeWeight))
                .ToDictionary(i => i.Key, i => i.ToList());

            ItemSaver.SaveBySize(itemsBySize);

            var itemsForMainPage = items.Where(MainPageManager.IsForMainPage);

            ItemSaver.SendItemsForMainPage(itemsForMainPage);
        }
    }
}